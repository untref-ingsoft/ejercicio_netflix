# Sistema de recomendación de series y películas

Implementar un sistema que permita obtener una colección de series y películas para un usuario determinado.
Las recomendaciones deben priorizarse en el siguiente orden:

1. Contenidos que se encuentren en la lista de contenidos a mirar del usuario
2. Contenidos con todas sus categorías coincidiendo con las categorías de un contenido que el usuario haya marcado con "Me gusta"
3. Contenidos nuevos (lanzados el último mes) que no hayan sido marcados con "No me gusta"
