class Content
  attr_reader :name, :categories, :release_date

  def initialize(name, type, categories, release_date)
    @categories = categories
    @name = name
    @release_date = release_date
  end

  def to_s
    "#{@name} (#{@type}). Categorías: #{@categories.join(', ')}"
  end
end
