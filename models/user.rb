class User
  def initialize(content_repository)
    @to_watch_list = []
    @liked_content = []
    @not_liked_content = []

    @content_repository = content_repository
  end

  def add_content_to_list_to_watch(content)
    @to_watch_list << content
  end

  def mark_content_as_liked(content)
    @liked_content << content
  end

  def mark_content_as_not_liked(content)
    @not_liked_content << content
  end

  def get_recommendations
    # TODO: implementar
  end
end
