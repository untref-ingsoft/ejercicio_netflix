class ContentRepository
  def initialize
    @contents = []
  end

  def get_by_name(content_name)
    @contents.find { |content| content.name = content_name }
  end

  def get_by_matching_categories(categories)
    @contents.select { |content| categories & content.categories == categories }
  end

  def insert(content)
    @contents << content
  end
end
